package kinterfacedebug

import (
	"fmt"

	"gitlab.com/project_falcon/kubeless/lib/payload"
	"gitlab.com/project_falcon/kubeless/lib/tools"
	k8s "k8s.io/client-go/kubernetes"
	appsV1 "k8s.io/client-go/kubernetes/typed/apps/v1"
	apiV1 "k8s.io/client-go/kubernetes/typed/core/v1"
	v1Beta1 "k8s.io/client-go/kubernetes/typed/extensions/v1beta1"

	//only for localdebug
	"flag"
	"path/filepath"

	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

const (
	service = "podDebug"
)

func GetNamepace() (*apiV1.NamespaceInterface, error) {
	var kubeconfig *string
	var clientset *k8s.Clientset

	var ns apiV1.NamespaceInterface

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kube", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kube", "", "absolute path to the kubeconfig file")
	}

	flag.NewFlagSet("kube", flag.ExitOnError)

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		fmt.Println("error in config")
		// tools.HandlerMessage(apiEvent, service, fmt.Sprintf("K8s: Cannot build config from FLAGS. Err: %v", err.Error()), 500, tools.ErrColor)
		return &ns, err
	}

	clientset, err = k8s.NewForConfig(config)
	if err != nil {
		fmt.Println("error in clientset")
		// tools.HandlerMessage(apiEvent, service, fmt.Sprintf("K8s: Cannot create  Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &ns, err
	}

	ns = clientset.CoreV1().Namespaces()

	return &ns, err
}

func getFlag(apiEvent *payload.APIData) (*k8s.Clientset, error) {
	var kubeconfig *string
	var clientset *k8s.Clientset

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}

	flag.NewFlagSet("kubeconfig", flag.ExitOnError)

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("K8s: Cannot build config from FLAGS. Err: %v", err.Error()), 500, tools.ErrColor)
		return clientset, err
	}

	clientset, err = k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("K8s: Cannot create  Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return clientset, err
	}

	return clientset, err
}

//GetAllInterfaces create interface to interact with k8s
func GetAllInterfaces(apiEvent *payload.APIData, namespace string) (*apiV1.PodInterface, *apiV1.ServiceInterface, *v1Beta1.IngressInterface, *apiV1.NodeInterface, *apiV1.ConfigMapInterface,*apiV1.SecretInterface, *appsV1.DeploymentInterface,  error) {
	clientset, err := getFlag(apiEvent)

	if err != nil {
		fmt.Println("Problem with FLAG")

	}

	// nameSpaveInterface := clientset.CoreV1().Namespaces()
	svcInterface := clientset.CoreV1().Services(namespace)
	podInterface := clientset.CoreV1().Pods(namespace)
	ingInterface := clientset.ExtensionsV1beta1().Ingresses(namespace)
	nodeInterface := clientset.CoreV1().Nodes()
	configMapInterface := clientset.CoreV1().ConfigMaps(namespace)
	secretInterfaace := clientset.CoreV1().Secrets(namespace)
	deployInterface := clientset.AppsV1().Deployments(namespace)

	return &podInterface, &svcInterface, &ingInterface, &nodeInterface, &configMapInterface, &secretInterfaace, &deployInterface, err
}
